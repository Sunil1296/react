import { shallow } from "enzyme";
import Calculator from "../components/calculator";

describe("<calculator>", () => {
  test("should render input HTML Dom Element", () => {
    const wrapper = shallow(<Calculator />);
    expect(wrapper.find("#inputBox").text()).toBeDefined();
  });
  test("should render input value", () => {
    const wrapper = shallow(<Calculator />);
    wrapper.find("#inputBox").simulate("change", { target: { value: "" } });
    expect(wrapper.find("#inputBox").get(0).props.value).toEqual("");
  });
  test("render the div Html Dom which display result", () => {
    const wrapper = shallow(<Calculator />);
    expect(wrapper.find("#displayresult").text()).toBe('');
  });
  test("render the  click event  when clicked on button1 and display the value", () => {
    const button1 = shallow(<Calculator />);
    button1.find("#btn1").simulate("click");
    expect(button1.find("#inputBox").text()).toContain("");
  });
  test("render the  click event  when clicked on button2 and display the value", () => {
    const button2 = shallow(<Calculator />);
    button2.find("#btn2").simulate("click");
    expect(button2.find("#inputBox").text()).toContain("");
  });
  test("render the  click event  when clicked on button3 and display the value", () => {
    const button3 = shallow(<Calculator />);
    button3.find("#btn3").simulate("click");
    expect(button3.find("#inputBox").text()).toContain("");
  });
  test("render the  click event  when clicked on buttonsub and display the value", () => {
    const subbutton = shallow(<Calculator />);
    subbutton.find("#btnsub").simulate("click");
    expect(subbutton.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on button4 and display the value", () => {
    const button4 = shallow(<Calculator />);
    button4.find("#btn4").simulate("click");
    expect(button4.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on button5 and display the value", () => {
    const button5 = shallow(<Calculator />);
    button5.find("#btn5").simulate("click");
    expect(button5.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on button6 and display the value", () => {
    const button6 = shallow(<Calculator />);
    button6.find("#btn6").simulate("click");
    expect(button6.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on buttonmult and display the value", () => {
    const multbutton = shallow(<Calculator />);
    multbutton.find("#btnmult").simulate("click");
    expect(multbutton.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on button7 and display the value", () => {
    const button7 = shallow(<Calculator />);
    button7.find("#btn7").simulate("click");
    expect(button7.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on button8 and display the value", () => {
    const button8 = shallow(<Calculator />);
    button8.find("#btn8").simulate("click");
    expect(button8.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on button9 and display the value", () => {
    const button9 = shallow(<Calculator />);
    button9.find("#btn9").simulate("click");
    expect(button9.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on buttondiv and display the value", () => {
    const divbutton = shallow(<Calculator />);
    divbutton.find("#btndiv").simulate("click");
    expect(divbutton.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on button0 and display the value", () => {
    const zerobutton = shallow(<Calculator />);
    zerobutton.find("#btn0").simulate("click");
    expect(zerobutton.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked on buttonadd and display the value", () => {
    const addbutton = shallow(<Calculator />);
    addbutton.find("#btnadd").simulate("click");
    expect(addbutton.find("#inputBox").text()).toEqual("");
  });
  test("render the  click event  when clicked and get the result in the resultinput", () => {
    const resultbutton = shallow(<Calculator />);
    resultbutton.find("#resultbtn").simulate("click");
    expect(resultbutton.find("#displayresult").text()).toEqual("");
  });
  test("render the  click event  when clicked and get the result in the resultinput", () => {
    const clearbutton = shallow(<Calculator />);
    clearbutton.find("#btnclear").simulate("click");
    expect(clearbutton.find("#inputBox").text()).toEqual("");
  });
  
});
