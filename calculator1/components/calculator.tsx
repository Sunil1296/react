import React, { useState } from "react";
//
const Calculator = () => {
  const [inputnumber, setInputnumber] = useState('');
  const [result, setResultvalue] = useState('');

  const caluculateNumber = (e: any) => {
    setInputnumber(e.target.value);
  };
  const calculateResult = () => {
    setResultvalue(eval(inputnumber));
  };
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="row">
            <input
              type="text"
              id="inputBox"
              value={inputnumber}
              onChange={caluculateNumber}
            />
            <div id="displayresult">{result}</div>
            <div className="row">
              <button
                id="btn9"
                onClick={() => setInputnumber(inputnumber + "9")}
              >
                9
              </button>
              <button
                id="btn8"
                onClick={() => setInputnumber(inputnumber + "8")}
              >
                8
              </button>
              <button
                id="btn7"
                onClick={() => setInputnumber(inputnumber + "7")}
              >
                7
              </button>
              <button
                id="btndiv"
                onClick={() => setInputnumber(inputnumber + "/")}
              >
                %
              </button>
            </div>
            <div className="row">
              <button
                id="btn6"
                onClick={() => setInputnumber(inputnumber + "6")}
              >
                6
              </button>
              <button
                id="btn5"
                onClick={() => setInputnumber(inputnumber + "5")}
              >
                5
              </button>
              <button
                id="btn4"
                onClick={() => setInputnumber(inputnumber + "4")}
              >
                4
              </button>
              <button
                id="btnmult"
                onClick={() => setInputnumber(inputnumber + "*")}
              >
                *
              </button>
            </div>
            <div className="row">
              <button
                id="btn3"
                onClick={() => setInputnumber(inputnumber + "3")}
              >
                3
              </button>
              <button
                id="btn2"
                onClick={() => setInputnumber(inputnumber + "2")}
              >
                2
              </button>
              <button
                id="btn1"
                onClick={() => setInputnumber(inputnumber + "1")}
              >
                1
              </button>
              <button
                id="btnsub"
                onClick={() => setInputnumber(inputnumber + "-")}
              >
                -
              </button>
            </div>
            <div className="row">
              <button
                id="btn0"
                onClick={() => setInputnumber(inputnumber + "0")}
              >
                0
              </button>
              <button
                id="btnadd"
                onClick={() => setInputnumber(inputnumber + "+")}
              >
                +
              </button>
              <button id="resultbtn" onClick={calculateResult}>
                =
              </button>
              <button id="btnclear" onClick={() => setInputnumber("")}>
                clear
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Calculator;
