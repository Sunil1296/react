import React from "react" 

const Todolist =(props)=>{
   
    return(
    <>
   
    <li className="shadow p-3 my-2 col-sm-12">{props.text}
    <button className="btn btn-danger  col-sm-2 offset-7"  onClick={
        ()=>{
            props.onSelect(props.id)
        }
    }>delete</button>
    </li>
   
    </>)
    
    
}

export default Todolist;
