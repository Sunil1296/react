import React, { useState } from "react";
import Todolist from "./Todo";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

function App() {
  const [inputlist, setinputlist] = useState("");
//
  const[items,listitem]=useState([]);
//
  const iteminput = (e) => {
    setinputlist(e.target.value);
  };
//
  const addlist=()=>{
      listitem((oldlist)=>{
        return [...oldlist,inputlist]
      })
      setinputlist("")
  }
  //
  const deletelist=(id)=>{
    console.log("delete");
    listitem((oldlist)=>{
      return oldlist.filter((arrayelement,index)=>{
        return index !== id ;
      })
    })
}
//
  return (
    <div className="App">
      <div className="main">
        <h1>Todo List</h1>
        <div className="container-fluid">
          <div className="row">
            <div className="col-9">
              <input
                type="text"
                className="form-control"
                value={inputlist}
                placeholder="Add Todo List"
                onChange={iteminput}
              />
            </div>
            <div className="col-2">
              <button className="btn btn-warning px-5 font-weight-bold" onClick={addlist}>
                Add
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="conatiner">
      <ol  className=" row">
        {
          items.map((inputvalue,index) => {
          return <Todolist  key={index} id={index} text={inputvalue} onSelect={deletelist} />
          })
        }
      </ol>
      </div>
    </div>
  );
}

export default App;
