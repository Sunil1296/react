import { useState } from "react";

import * as math from "mathjs";

import "./App.css";
import Button from "./components/Button";
import Input from "./components/Input";
// hjj

const App = () => {
  const [text, settext] = useState("");
  const [result, setresult] = useState("");

  const addtext = (val) => {
    settext((text) => [...text, val]);
  };

  const calculateResult = () => {
    const input = text.join(""); // Remove commas between nors
    // console.log(text, input)
    setresult(math.evaluate(input));
  };

  const resetinput = () => {
    settext("");
    setresult("");
  };

  

  return (
    <div className="App">
      <div className="calc">
      <h1> Basic  Calculator</h1>
        <Input text={text} result={result} />
        <div className="row">
          <Button symbol="7" handleClick={addtext} />
          <Button symbol="8" handleClick={addtext} />
          <Button symbol="9" handleClick={addtext} />
          <Button symbol="/" handleClick={addtext} />
        </div>
        <div className="row">
          <Button symbol="4" handleClick={addtext} />
          <Button symbol="5" handleClick={addtext} />
          <Button symbol="6" handleClick={addtext} />
          <Button symbol="*" handleClick={addtext} />
        </div>
        <div className="row">
          <Button symbol="1" handleClick={addtext} />
          <Button symbol="2" handleClick={addtext} />
          <Button symbol="3" handleClick={addtext} />
          <Button symbol="+"handleClick={addtext} />
        </div>
        <div className="row">
          <Button symbol="0" handleClick={addtext} />
          <Button symbol="Clear" handleClick={resetinput}/>
          <Button symbol="-" handleClick={addtext} />
          <Button symbol="=" handleClick={calculateResult} />
        </div>
      </div>
    </div>
  );
};

export default App;